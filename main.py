import discord
import os

from dotenv import load_dotenv
from discord.ext import commands
from webserver import keep_alive

client = commands.Bot(command_prefix = ["."], help_command=None)

load_dotenv()

for f in os.listdir("./cogs"):
    if f.endswith(".py"):
        client.load_extension("cogs." + f[:-3])

@client.event
async def on_ready():
    await client.change_presence(status=discord.Status.online, activity=discord.Game('Lightning'))
    print('Bot is now running.')

keep_alive()

TOKEN = os.environ.get("BOT_TOKEN")

client.run(TOKEN)